# API -- FIRST DATABASE PROJECT

## Project on browser
![Project on browser](https://gitlab.com/sebastian.baez/api-first-database-project/-/raw/master/gl-images/on_browser.png "Project on browser")


###### What does the project do?
> This is a CRUD implement to manipulate students information. It is able to:
- Create students on database.
- Read the students information and show on browser.
- Update the students information.
- Delete a existed student on database.


## Database commands
![Database commands](https://gitlab.com/sebastian.baez/api-first-database-project/-/raw/master/gl-images/database_commands.png "Database commands")