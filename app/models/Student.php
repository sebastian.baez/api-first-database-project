<?php
namespace app\models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model {
    protected $table = 'students';
    public $timestamps = false;
}
