<?php

namespace app\models;

class Services{

    private static $first_name;
    private static $last_name;
    private static $career;
    private static $student_code;
    private static $semester;

    public function createStudent($first_name , $last_name , $career , $student_code , $semester){
        self::$first_name = $first_name;
        self::$first_name = $last_name;
        self::$first_name = $career;
        self::$first_name = $student_code;
        self::$first_name = $semester;

        $query = 'INSERT INTO students (first_name,last_name,career,student_code,semester) VALUES (:first_name,:last_name,:career,:student_code,:semester)';

        $statement = $this->pdo->prepare($query);

        $statement->bindValue(':first_name' , self::$first_name);
        $statement->bindValue(':last_name' , self::$last_name);
        $statement->bindValue(':career' , self::$career);
        $statement->bindValue(':student_code' , self::$student_code);
        $statement->bindValue(':semester' , self::$semester);

        $statement->execute();

        return $this->pdo->lastInsertId('student_id_seq');
    }

}