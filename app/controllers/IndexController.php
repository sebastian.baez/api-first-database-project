<?php

namespace app\controllers;

use app\models\Student;

class IndexController extends BaseController {

    public function getStudents(){
        $students = Student::all();

        return $this->renderHTML('index.twig' , [
            'students' => $students,
        ]);
    }

}