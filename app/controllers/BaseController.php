<?php
namespace app\controllers;

use Laminas\Diactoros\Response\HtmlResponse;

class BaseController {

    protected $template_engine;

    public function __construct(){
        $loader = new \Twig\Loader\FilesystemLoader('../views');
        $this->template_engine = new \Twig\Environment($loader, [
            'debug' => true,
            'cache' => false,
        ]);

    }

    public function renderHTML($template , $data = []){
        return new HtmlResponse($this->template_engine->render($template , $data));
    }
}