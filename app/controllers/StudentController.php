<?php

namespace app\controllers;

use app\models\Student;
use Laminas\Diactoros\Response\RedirectResponse;

class StudentController extends BaseController {

    public function getTemplate(){
        return $this->renderHTML('AddStudent.twig');
    }

    public function addStudent($request){

        if ($request->getMethod() == 'POST') {
            $data = $request->getParsedBody();

            $student = new Student;
            $student->first_name = $data['first_name'];
            $student->last_name = $data['last_name'];
            $student->career = $data['career'];
            $student->student_code = $data['student_code'];
            $student->semester = $data['semester'];
            $student->save();

            #var_dump($data);
        }

        return $this->renderHTML('AddStudent.twig');

    }

    public function deleteStudent($request){
        $id_student = $request->getQueryParams()['id'];
        
        $student = Student::find($id_student);
        $student->delete();
 
        return new RedirectResponse('/api/');

        #var_dump(header('Location: http://localhost/api/'));
    }
}

/* if (!empty($_POST)) {
    $student = new Student();
    $student->first_name = $_POST['first_name'];
    $student->first_name = $_POST['last_name'];
    $student->first_name = $_POST['career'];
    $student->first_name = $_POST['student_code'];
    $student->first_name = $_POST['semester'];
    $student->save();
}
 */