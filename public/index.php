<?php
ini_set('display_errors' , 1);
ini_set('display_starup_error' , 1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as Capsule;
use Aura\Router\RouterContainer;

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'pgsql',
    'host'      => 'localhost',
    'database'  => 'my_database',
    'username'  => 'postgres',
    'password'  => 'postgres',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '', 
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();
/*
$route = $_GET['route'] ?? '/';

if ($route == '/'){
    require '../index.php';
}elseif($route == 'AddStudent'){
    require '../AddStudent.php';
}
*/

$request = Laminas\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

$routerContainer = new RouterContainer();

$map = $routerContainer->getMap();

$map->get('index', '/api/' , [
    'controller' => 'app\controllers\IndexController' ,
    'action' => 'getStudents' ,
]  );
$map->get('newTemplate', '/api/new' , [
    'controller' => 'app\controllers\StudentController' ,
    'action' => 'getTemplate' ,
]);
$map->post('new', '/api/new' , [
    'controller' => 'app\controllers\StudentController' ,
    'action' => 'addStudent' ,
]);
$map->get('delete-student', '/api/delete' , [
    'controller' => 'app\controllers\StudentController' ,
    'action' => 'deleteStudent' ,
]);



$matcher = $routerContainer->getMatcher();

$route = $matcher->match($request);

if ($route) {

    $handler = $route->handler;

    $controller_name = $handler['controller'];
    $action_name = $handler['action'];
    
    $controller = new $controller_name;
    
    $response = $controller->$action_name($request);

    foreach ($response->getHeaders() as $name => $values) {
        foreach ($values as $value) {
            header(sprintf('%s: %s', $name , $value) , false);
        }
    }

    http_response_code($response->getStatusCode());


    #var_dump($response);
    echo $response->getBody();
    #var_dump($response);
    #require $route->handler;
 
}



?>